DroidSieve
=========

Despite recent advances, obfuscated malware can still evade detection by automated systems that have to analyze thousands of apps per day. To address this challenge, we propose DroidSieve, an Android malware classifier based on static analysis that is fast, accurate, and resilient to obfuscation. For a given app, DroidSieve first decides whether the app is malicious and, if so, classifies it as belonging to a family of related malware. DroidSieve exploits obfuscation-invariant features and artifacts introduced by obfuscation mechanisms used in malware. This repository contains the feature extraction routines used in DroidSieve. 

This repository contains part of the framework used to extract features for DroidSieve. More details can be found in the paper here, although not all the features returned by this framework were used in the paper (in particular, amoung others, string features were ignored although they remain in the code for legacy reasons): 

http://www0.cs.ucl.ac.uk/staff/G.SuarezdeTangil/papers/2017codaspy-droidsieve.pdf

```
@inproceedings{suarez2017droidsieve,
  title={DroidSieve: Fast and accurate classification of obfuscated android malware},
  author={Suarez-Tangil, Guillermo and Dash, Santanu Kumar and Ahmadi, Mansour and Kinder, Johannes and Giacinto, Giorgio and Cavallaro, Lorenzo},
  booktitle={Proceedings of the Seventh ACM on Conference on Data and Application Security and Privacy},
  pages={309--320},
  year={2017},
  organization={ACM}
}
```


Important Note 
----
As described in the paper, the ML algorithms used in DroidSieve used feature selection. This is a very important step to remove irrelevant features. If you retain all the features and train DroidSieve with a non-representative dataset, the classifier will underperform in real-world settings. It is also important to note that numeric features will have to be scaled to obtain the best performance. 

Also, we emphasize again that not all the features returned by this framework were used in the paper. In particular, amoung others, string features were ignored although they remain in the code for legacy reasons. String features are those that are pre-pended by the following categories: 'string', 'version', 'cert', etc. 


 
Version
----
 
1.0
 
Tech
-----------
 
Alterdroid uses the following Operating Systems:
 
* [Ubuntu Server or Desktop]
 
 
Install Androguard
--------------
 
Follow the instructions here: https://code.google.com/p/androguard/
 
The steps are as follows:
 
```sh
sudo apt-get install mercurial python python-setuptools g++
 
sudo apt-get install python-dev libbz2-dev libmuparser-dev libsparsehash-dev python-ptrace python-pygments python-pydot graphviz liblzma-dev libsnappy-dev
 
sudo apt-get install python-dev libbz2-dev libmuparser-dev libsparsehash-dev python-ptrace python-pygments python-pydot graphviz liblzma-dev libsnappy-dev
 
hg clone https://androguard.googlecode.com/hg/ androguard
```
 
Virtualenv
----------

For an optimal extraction DroidSieve requires Python 2.7.11 (earlier versions of python are not able to unzip many Android APKs). Furthermore, there are a number of dependencies that are paramount to extract certain features (such as python-magic, for those related to file inconsistencies). Thus, it is recommended to run DroidSieve from the python virtual environment provided: 

```sh
tar xvf env_fdfeatures.tar.gz 
source env_fdfeatures/bin/activate
```


Install Phyton
--------------
 
Follow the instructions here: https://code.google.com/p/pythonxy/
 
The steps are as follows:
 
```sh
cd ~/Downloads/

wget http://python.org/ftp/python/2.7.11/Python-2.7.11.tgz

tar -xvf Python-2.7.11.tgz

cd Python-2.7.11

./configure

make

sudo make install
```
 
 
Other dependencies
--------------
 
```sh
sudo apt-get install python-imaging

sudo apt-get install python-magic

sudo apt-get install imagemagick
```
 
Examples for execution
--------------


DroidScieve requires a pickle file containing meta-information pointing to the samples. To generate this pickle file you can use the following script located at ./preprocessing folder

```sh
python genearete_metainfo_pickle.py 
```

Then execute the extraction of features:

```sh
python feature_extraction.py --malicious --dataset drebin 

```